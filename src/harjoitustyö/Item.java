/*
 * Program: TIMO
 * Filename: Item.java
 * Author: Joonas Raassina
 * Content: The Item java class
 */
package harjoitustyö;

import static java.lang.Boolean.TRUE;


public class Item {
    private String name;
    private double size;
    private double mass;
    private boolean fragile;
    
    public Item(String a, double b, double c, boolean d) {
        name = a;
        size = b;
        mass = c;
        fragile = d;
    }
    
    @Override
    public String toString() { // returns the name, size, mass and fragility of an item to the other classes 
        String fragility;
        if (fragile == TRUE) {
            fragility = "kyllä";
        }
        else {
            fragility = "ei";
        }
        return name + " (Koko: " + size + " dm³, paino: " + mass + " kg, särkyvää: " + fragility +")";
    }
    
    public double getSize() { // returns the size of an item to the other classes
        return size;
    }
    
    public double getMass() { // returns the mass of an item to the other classes
        return mass;
    }
    
    public boolean getFragile() { // returns the fragility information of an item to the other classes
        return fragile;
    }
}

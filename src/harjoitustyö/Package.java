/*
 * Program: TIMO
 * Filename: Package.java
 * Author: Joonas Raassina
 * Content: The abstract java class "Package" and its sub-classes "FirstClass", "SecondClass" and "ThirdClass"
 */
package harjoitustyö;

import static java.lang.Boolean.FALSE;
import java.util.ArrayList;

abstract class Package {
    protected int id;
    protected double distance; // the distance between the start and destination SmartPost lockers
    protected boolean delivered; // the delivery status of a package
    protected SmartPost sp_start; // the start SmartPost locker
    protected SmartPost sp_destination; // the destination SmartPost locker
    protected Item item;
    protected int packageClass;
    
    public Package(int a, double b, SmartPost c, SmartPost d, Item e, int f) {
        id = a;    
        distance = b;
        sp_start = c;
        sp_destination = d;
        item = e;
        packageClass = f;
        delivered = FALSE;
    }
    
    @Override
     public String toString() { // returns the information of a selected package to the other classes
        return "ID: " + id + " Pakettiluokka: " + packageClass + " Lähtöpaikka: " + sp_start.getLocation() + " Määränpää: " + sp_destination.getLocation() + " Sisältö: " + item.toString();
     }
}

class FirstClass extends Package {
    
    public FirstClass(int a, double b, SmartPost c, SmartPost d, Item e, int f) {
        super(a,b,c,d,e,f);
    }
    
}

class SecondClass extends Package {
    
    public SecondClass(int a, double b, SmartPost c, SmartPost d, Item e, int f) {
        super(a,b,c,d,e,f);
    }
}

class ThirdClass extends Package {
    
    public ThirdClass(int a, double b, SmartPost c, SmartPost d, Item e, int f) {
        super(a,b,c,d,e,f);
    }
    
}

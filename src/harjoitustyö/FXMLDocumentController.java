/*
 * Program: TIMO
 * Filename: FXMLDocumentController.java
 * Author: Joonas Raassina
 * Content: The FXMLDocumentController java class which offers an FXML controller for the tab "Etusivu"
 */
package harjoitustyö;

import java.io.IOException;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.web.WebView;
import javafx.stage.Stage;



public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button addMapButton;
    @FXML
    private ComboBox<String> combo;
    @FXML
    private WebView mapView;
    @FXML
    private Button resetMapButton;
    @FXML
    private Button sendButton;
    @FXML
    private Button createButton;
    @FXML
    private ComboBox<Package> combo1;
    @FXML
    private Button closeButton;
    
    Main m = null;
        
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        m = Main.getInstance(); // gets the instance of the "Main" class
        
        ArrayList<String> city_array = m.getCityArray(); //gets the "city_array" from the "Main" class
        for (String s : city_array) { // initializes the ComboBox
            combo.getItems().add(s);
        } 
        
       combo.getSelectionModel().selectFirst();
       mapView.getEngine().load(getClass().getResource("index.html").toExternalForm()); // loads the html file which contains the javascript functions
        
    }    

    @FXML
    private void addMapAction(ActionEvent event) { // shows the SmartPost lockers of the selected city in the map view
        ArrayList<SmartPost> smartpost_array = m.getSmartPostArray();
        for (SmartPost s : smartpost_array) {
            if (s.getCity().equals(combo.valueProperty().getValue())) {
                mapView.getEngine().executeScript("document.goToLocation('" + s.getLocation() + "','" + s.getInfo() + "','red')");
            }
        }
    }

    @FXML
    private void resetMapAction(ActionEvent event) { // removes the paths in the map view
        mapView.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void sendAction(ActionEvent event) { // sends the selected package and shows its route as a path in the map view
        // creates a new ArrayList and adds the coordinates of the package route to the list
        ArrayList route = new ArrayList<Float>();
        route.add(combo1.valueProperty().getValue().sp_start.getLat());
        route.add(combo1.valueProperty().getValue().sp_start.getLng());
        route.add(combo1.valueProperty().getValue().sp_destination.getLat());
        route.add(combo1.valueProperty().getValue().sp_destination.getLng());
        
        int packageClass = 0;
        if (combo1.valueProperty().getValue() instanceof FirstClass) {
            packageClass = 1;
        }
        if (combo1.valueProperty().getValue() instanceof SecondClass) {
            packageClass = 2;
        }
        if (combo1.valueProperty().getValue() instanceof ThirdClass) {
            packageClass = 3;
        }
        mapView.getEngine().executeScript("document.createPath(" + route + ",'red'," + packageClass + ")"); // executes a javascript function for drawing a path in the map view
        combo1.valueProperty().getValue().delivered = TRUE; // changes the delivery status of the package sent
        updateCombo(); // updates the ComboBox
    }

    @FXML
    private void createAction(ActionEvent event) { // opens a new window for creating a new package
        try {
            Stage packageView = new Stage();
            packageView.setTitle("Uusi paketti");
            Parent page = FXMLLoader.load(getClass().getResource("FXMLPackageView.fxml"));
            Scene scene = new Scene(page);
            packageView.setScene(scene);
            packageView.setOnHidden(e -> {updateCombo();}); // calls the function "updateCombo" as the new window is closed
            packageView.show();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

        
    private void updateCombo() { // updates the ComboBox
        combo1.getItems().removeAll(combo1.getItems());
        ArrayList<Package> storage_array = m.getStorageArray();
        for (Package p : storage_array) {
            if (p.delivered == FALSE) {
                combo1.getItems().add(p);
            }
        }
    }

    @FXML
    private void closeAction(ActionEvent event) throws IOException { // calls the logging function and then exits
        m.printLog(); // calls the logging function located in the class "Main"
        Platform.exit();
        System.exit(0);
    }
    
}

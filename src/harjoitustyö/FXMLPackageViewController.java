/*
 * Program: TIMO
 * Filename: FXMLPackageViewController.java
 * Author: Joonas Raassina
 * Content: The FXMLPackageViewController java class which offers an FXML controller for the window in which a user can create a new package
 */
package harjoitustyö;

import harjoitustyö.FXMLDocumentController;
import harjoitustyö.FXMLNotificationController;
import harjoitustyö.Item;
import harjoitustyö.Main;
import harjoitustyö.SmartPost;
import java.io.IOException;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.web.WebView;
import javafx.stage.Stage;


public class FXMLPackageViewController implements Initializable {

    @FXML
    private ComboBox<Item> itemCombo;
    @FXML
    private TextField itemNameField;
    @FXML
    private TextField itemSizeField;
    @FXML
    private TextField itemMassField;
    @FXML
    private CheckBox fragileBox;
    @FXML
    private RadioButton firstClassRadio;
    @FXML
    private RadioButton secondClassRadio;
    @FXML
    private RadioButton thirdClassRadio;
    @FXML
    private Button showInfoButton;
    @FXML
    private ComboBox<String> startCityCombo;
    @FXML
    private ComboBox<SmartPost> startPointCombo;
    @FXML
    private ComboBox<String> destinationCityCombo;
    @FXML
    private ComboBox<SmartPost> destinationPointCombo;
    @FXML
    private Button cancelButton;
    @FXML
    private Button createPackageButton;
    @FXML
    private WebView webView;
    
    Main m = null;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        m = Main.getInstance(); //gets the instance of the "Main" class
        ArrayList<Item> item_array = m.getItemArray(); //gets the "item_array" from the "Main" class
        for (Item i : item_array) { // initializes the ComboBox in which the items are shown
            itemCombo.getItems().add(i);
        } 
        
        ArrayList<String> city_array = m.getCityArray(); //gets the "city_array" from the "Main" class
        for (String s : city_array) { // initializes the ComboBoxes in which the SmartPost cities are shown
            startCityCombo.getItems().add(s);
            destinationCityCombo.getItems().add(s);
        }
       
        ToggleGroup group = new ToggleGroup(); // initializes and groups the package class toggles
        firstClassRadio.setToggleGroup(group);
        secondClassRadio.setToggleGroup(group);
        thirdClassRadio.setToggleGroup(group);
        
        webView.getEngine().load(getClass().getResource("index.html").toExternalForm()); // loads the html file which contains the javascript functions
    }    

    @FXML
    private void showInfoAction(ActionEvent event) { // opens the new window which offers a user information about package classes
        try {
            Stage packageView = new Stage();
            packageView.setTitle("Tietoa pakettiluokista");
            Parent page = FXMLLoader.load(getClass().getResource("FXMLPackageInfo.fxml"));
            Scene scene = new Scene(page);
            packageView.setScene(scene);
            packageView.show();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void cancelAction(ActionEvent event) { // closes the window as the cancel button is selected
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void createPackageAction(ActionEvent event) { // creates a new package
        if (startPointCombo.getSelectionModel().isEmpty() || destinationPointCombo.getSelectionModel().isEmpty()) { // if either one or both of the ComboBoxes showing the start and destination SmartPost lockers are empty, the pop-up notification window is shown
            FXMLLoader Loader = new FXMLLoader();
            Loader.setLocation(getClass().getResource("FXMLNotification.fxml"));
            try {
                Loader.load();
            }
            catch (IOException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
            FXMLNotificationController notification = Loader.getController();
            notification.setText("Määritä lähtöpaikka ja määränpää!");
            Parent p = Loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.show();
        }
        else if (!firstClassRadio.isSelected() && !secondClassRadio.isSelected() && !thirdClassRadio.isSelected()) { // if no toggle is selected, the pop-up notification window is shown
            FXMLLoader Loader = new FXMLLoader();
            Loader.setLocation(getClass().getResource("FXMLNotification.fxml"));
            try {
                Loader.load();
            }
            catch (IOException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
            FXMLNotificationController notification = Loader.getController();
            notification.setText("Määritä pakettiluokka!");
            Parent p = Loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.show();
        }
        else if (itemCombo.getSelectionModel().isEmpty() && (itemNameField.getText().trim().isEmpty() || itemSizeField.getText().trim().isEmpty() || itemMassField.getText().trim().isEmpty())) { // if no item in the item ComboBox is selected and some or all of the fields for creating a new item are empty, the pop-up notification window is shown
            FXMLLoader Loader = new FXMLLoader();
            Loader.setLocation(getClass().getResource("FXMLNotification.fxml"));
            try {
                Loader.load();
            }
            catch (IOException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
            FXMLNotificationController notification = Loader.getController();
            notification.setText("Valitse esine pudotusvalikosta tai luo uusi esine!");
            Parent p = Loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.show();
        }
        else if (startPointCombo.valueProperty().getValue().toString().equals(destinationPointCombo.valueProperty().getValue().toString())) { // if the start SmartPost locker and the destination SmartPost locker are the same, the pop-up notification window is shown
            FXMLLoader Loader = new FXMLLoader();
            Loader.setLocation(getClass().getResource("FXMLNotification.fxml"));
            try {
                Loader.load();
            }
            catch (IOException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
            FXMLNotificationController notification = Loader.getController();
            notification.setText("Määränpään on oltava eri kuin lähtöpisteen!");
            Parent p = Loader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(p));
            stage.show();
        }
        else {
            ArrayList<Float> route = new ArrayList<Float>(); // initializes an ArrayList for the coordinates of the package route
            // the coordinates are added to the ArrayList
            route.add(startPointCombo.valueProperty().getValue().getLat());
            route.add(startPointCombo.valueProperty().getValue().getLng());
            route.add(destinationPointCombo.valueProperty().getValue().getLat());
            route.add(destinationPointCombo.valueProperty().getValue().getLng());
            Item i;
            double distance = (double) (webView.getEngine().executeScript("document.createPath(" + route + ",'red',1)")); // the distance between the start SmartPost locker and the destination SmartPost locker is calculated
            if (itemCombo.getSelectionModel().isEmpty()) { // if the item ComboBox is empty, a new item object is created
                boolean d = FALSE;
                if (fragileBox.isSelected()) {
                    d = TRUE;
                }
                i = m.createNewItem(itemNameField.getText(),Double.parseDouble(itemSizeField.getText()),Double.parseDouble(itemMassField.getText()),d);
            }
            else {
                i = itemCombo.valueProperty().getValue();
            }
            if (firstClassRadio.isSelected()) { // actions if the first class toggle is selected
                
                if (i.getFragile() == TRUE) { // if the item is fragile, the pop-up notification window is shown
                    FXMLLoader Loader = new FXMLLoader();
                    Loader.setLocation(getClass().getResource("FXMLNotification.fxml"));
                    try {
                        Loader.load();
                    }
                    catch (IOException ex) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    FXMLNotificationController notification = Loader.getController();
                    notification.setText("1. pakettiluokka ei sovellu särkyvien tavaroiden kuljetukseen!");
                    Parent p = Loader.getRoot();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(p));
                    stage.show();
                }
                else if ((double) (webView.getEngine().executeScript("document.createPath(" + route + ",'red',1)")) > 150) { // if the distance between the start SmartPost locker and the destination SmartPost locker is more than 150 km, the pop-up notification window is shown
                    FXMLLoader Loader = new FXMLLoader();
                    Loader.setLocation(getClass().getResource("FXMLNotification.fxml"));
                    try {
                        Loader.load();
                    }
                    catch (IOException ex) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    FXMLNotificationController notification = Loader.getController();
                    notification.setText("1. pakettiluokassa kuljetusmatka on enintään 150 km!");
                    Parent p = Loader.getRoot();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(p));
                    stage.show();
                }
                else if (i.getSize() > 80) { // if the size of the item selected is more than 80 dm³, the pop-up notification window is shown
                    FXMLLoader Loader = new FXMLLoader();
                    Loader.setLocation(getClass().getResource("FXMLNotification.fxml"));
                    try {
                        Loader.load();
                    }
                    catch (IOException ex) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    FXMLNotificationController notification = Loader.getController();
                    notification.setText("1. pakettiluokassa lähetyksen tilavuus on rajoitettu arvoon 80 dm³!");
                    Parent p = Loader.getRoot();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(p));
                    stage.show();
                }
                else if (i.getMass() > 35) { // if the mass of the item selected is more than 35 kg, the pop-up notification window is shown
                    FXMLLoader Loader = new FXMLLoader();
                    Loader.setLocation(getClass().getResource("FXMLNotification.fxml"));
                    try {
                        Loader.load();
                    }
                    catch (IOException ex) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    FXMLNotificationController notification = Loader.getController();
                    notification.setText("1. pakettiluokassa lähetyksen paino on rajoitettu arvoon 35 kg!");
                    Parent p = Loader.getRoot();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(p));
                    stage.show();
                }
                else { // if the item meets all the requirements of the first class package, a new first class package is created and the window is closed
                    int packageClass = 1;
                    if (itemCombo.getSelectionModel().isEmpty()) { // if a new item was created, it is added to the ArrayList "item_array" located in the class "Main"
                        m.addItemToArray(i);
                    }
                    m.addPackageToStorage(i,startPointCombo.valueProperty().getValue(),destinationPointCombo.valueProperty().getValue(),distance,packageClass);
                    Stage stage = (Stage) createPackageButton.getScene().getWindow();
                    stage.close();
                }
            }
        
        
            else if (secondClassRadio.isSelected()) { // actions if the second class toggle is selected
                if (i.getSize() > 50) { // if the size of the item selected is more than 50 dm³, the pop-up notification window is shown
                    FXMLLoader Loader = new FXMLLoader();
                    Loader.setLocation(getClass().getResource("FXMLNotification.fxml"));
                    try {
                        Loader.load();
                    }
                    catch (IOException ex) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    FXMLNotificationController notification = Loader.getController();
                    notification.setText("2. pakettiluokassa lähetyksen tilavuus on rajoitettu arvoon 50 dm³!");
                    Parent p = Loader.getRoot();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(p));
                    stage.show();
                }
                else if (i.getMass() > 35) { // if the mass of the item selected is more than 35 kg, the pop-up notification window is shown
                    FXMLLoader Loader = new FXMLLoader();
                    Loader.setLocation(getClass().getResource("FXMLNotification.fxml"));
                    try {
                        Loader.load();
                    }
                    catch (IOException ex) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    FXMLNotificationController notification = Loader.getController();
                    notification.setText("2. pakettiluokassa lähetyksen paino on rajoitettu arvoon 35 kg!");
                    Parent p = Loader.getRoot();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(p));
                    stage.show();
                    }
                else { // if the item meets all the requirements of the second class package, a new second class package is created and the window is closed
                    if (itemCombo.getSelectionModel().isEmpty()) { // if a new item was created, it is added to the ArrayList "item_array" located in the class "Main"
                        m.addItemToArray(i);
                    }
                    int packageClass = 2;
                    m.addPackageToStorage(i,startPointCombo.valueProperty().getValue(),destinationPointCombo.valueProperty().getValue(),distance,packageClass);
                    Stage stage = (Stage) createPackageButton.getScene().getWindow();
                    stage.close();
                }
            }

            else if (thirdClassRadio.isSelected()) {  // actions if the third class toggle is selected
                if (i.getSize() < 80) { // if the size of the item selected is less than 80 dm³, the pop-up notification window is shown
                    FXMLLoader Loader = new FXMLLoader();
                    Loader.setLocation(getClass().getResource("FXMLNotification.fxml"));
                    try {
                        Loader.load();
                    }
                    catch (IOException ex) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    FXMLNotificationController notification = Loader.getController();
                    notification.setText("3. pakettiluokassa lähetyksen vähimmäistilavuus on 80 dm³!");
                    Parent p = Loader.getRoot();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(p));
                    stage.show();
                }
                else if (i.getFragile() == TRUE) { // if the item is fragile, the pop-up notification window is shown
                    FXMLLoader Loader = new FXMLLoader();
                    Loader.setLocation(getClass().getResource("FXMLNotification.fxml"));
                    try {
                        Loader.load();
                    }
                    catch (IOException ex) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    FXMLNotificationController notification = Loader.getController();
                    notification.setText("3. pakettiluokka ei sovellu särkyvien tavaroiden kuljetukseen!");
                    Parent p = Loader.getRoot();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(p));
                    stage.show();
                    }
                else if (i.getMass() > 100) { // if the mass of the item selected is more than 100 kg, the pop-up notification window is shown
                    FXMLLoader Loader = new FXMLLoader();
                    Loader.setLocation(getClass().getResource("FXMLNotification.fxml"));
                    try {
                        Loader.load();
                    }
                    catch (IOException ex) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    FXMLNotificationController notification = Loader.getController();
                    notification.setText("3. pakettiluokassa lähetyksen paino on rajoitettu arvoon 100 kg!");
                    Parent p = Loader.getRoot();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(p));
                    stage.show();
                    }
                else { // if the item meets all the requirements of the third class package, a new third class package is created and the window is closed
                    if (itemCombo.getSelectionModel().isEmpty()) { // if a new item was created, it is added to the ArrayList "item_array" located in the class "Main"
                        m.addItemToArray(i);
                    }
                    int packageClass = 3;
                    m.addPackageToStorage(i,startPointCombo.valueProperty().getValue(),destinationPointCombo.valueProperty().getValue(),distance,packageClass);
                    Stage stage = (Stage) createPackageButton.getScene().getWindow();
                    stage.close();
                }
            }
        }
    }

    @FXML
    private void startCityAction(ActionEvent event) { // as a user selects a city in the ComboBox "startCityCombo", the ComboBox "startPointCombo" showing the SmartPost lockers in the selected city is initialized
        startPointCombo.getItems().removeAll(startPointCombo.getItems());
        ArrayList<SmartPost> smartpost_array = m.getSmartPostArray();
        for (SmartPost s : smartpost_array) {
           if (s.getCity().equals(startCityCombo.valueProperty().getValue())) {
                startPointCombo.getItems().add(s);
            }
        }
    }

    @FXML
    private void destinationCityAction(ActionEvent event) { // as a user selects a city in the ComboBox "destinationCityCombo", the ComboBox "destinationPointCombo" showing the SmartPost lockers in the selected city is initialized
        destinationPointCombo.getItems().removeAll(destinationPointCombo.getItems());
        ArrayList<SmartPost> smartpost_array = m.getSmartPostArray();
        for (SmartPost s : smartpost_array) {
           if (s.getCity().equals(destinationCityCombo.valueProperty().getValue())) {
                destinationPointCombo.getItems().add(s);
            }
        }
    }
    
}

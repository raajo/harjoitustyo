/*
 * Program: TIMO
 * Filename: Storage.java
 * Author: Joonas Raassina
 * Content: The Storage java class which is in charge of storing the packages
 */
package harjoitustyö;

import java.util.ArrayList;

public class Storage {
    static private Storage s = null;
    private ArrayList<Package> package_array = new ArrayList<Package>(); // initializes the ArrayList where the packages are stored
    
    static public Storage getInstance() {
        if (s == null)
            s = new Storage();
        return s;
    }
    
    public ArrayList getPackageArray() { //returns the ArrayList "package_array" to the class "Main"
        return package_array;
    }
    
    public void addPackageToStorage(Package p) { //adds the package to the ArrayList "package_array"
        package_array.add(p);
    }
    
}

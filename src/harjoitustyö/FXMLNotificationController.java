/*
 * Program: TIMO
 * Filename: FXMLNotificationController.java
 * Author: Joonas Raassina
 * Content: The FXMLDocumentController java class which offers an FXML controller for the notification pop-up window
 */
package harjoitustyö;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;


public class FXMLNotificationController implements Initializable {

    @FXML
    private Label notificationLabel;
    @FXML
    private Button closeButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void closeAction(ActionEvent event) { // closes the window as the close button is selected
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
    
    public void setText(String s) { // sets the text in the label
        this.notificationLabel.setText(s);
    }
    
}

/*
 * Program: TIMO
 * Filename: Main.java
 * Author: Joonas Raassina
 * Content: The Main java class which links the package storage and XML information to the GUI
 */
package harjoitustyö;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class Main {
    static private Main m = null;
    private ArrayList<Item> item_array = new ArrayList<Item>(); // initializes an ArrayList for items
    DataBuilder d = DataBuilder.getInstance(); // gets the instance of the "DataBuilder" class
    Storage s = Storage.getInstance(); // gets the instance of the "Storage" class
    private int id = 0; // initializes the ID number of packages created
    
    static public Main getInstance() {
        if (m == null)
            m = new Main();
        return m;
    }
    
    private Main() { //creates a few items and adds them to the item_array
        item_array.add(new Item("My Little Pony -figuuri",23.76,1,false));
        item_array.add(new Item("Kauniit ja rohkeat DVD-paketti",30,2,false));
        item_array.add(new Item("Iittala lasimaljakko",35,2,true));
        item_array.add(new Item("Bosch MegaHot -mikroaaltouuni",127.44,10,true));
        item_array.add(new Item("Iso ja kestävä tavara",127.44,10,false));
        
    }
    
    public ArrayList getItemArray() { // returns the ArrayList "item_array" to the other classes
        return item_array;
    }
    
    public ArrayList getCityArray() { // returns the ArrayList "city_array" located in the class "DataBuilder" to the other classes
        return d.getCityArray();
    }
    
    public ArrayList getSmartPostArray() { // returns the ArrayList "smartpost_array" located in the class "DataBuilder" to the other classes
        return d.getSmartPostArray();
    }
    
    public void addPackageToStorage(Item i, SmartPost sp_s, SmartPost sp_d, double distance, int packageClass) { // adds the created package to the storage by calling the method "addPackageToStorage" located in the class "Storage"
        id++;
        if (packageClass == 1) {
            s.addPackageToStorage(new FirstClass(id,distance,sp_s,sp_d,i,packageClass));
        }
        if (packageClass == 2) {
            s.addPackageToStorage(new SecondClass(id,distance,sp_s,sp_d,i,packageClass));
        }
        if (packageClass == 3) {
            s.addPackageToStorage(new ThirdClass(id,distance,sp_s,sp_d,i,packageClass));
        }
    }
    
    public ArrayList getStorageArray() { // returns the ArrayList "package_array" located in the class "Storage" to the other classes
        return s.getPackageArray();
    }
    
    public Item createNewItem(String a, double b, double c, boolean d) { // creates a new object "Item"
        Item i  = new Item(a,b,c,d);
        return i;
    }
    
    public void addItemToArray(Item i) { // adds the created item to the ArrayList "item_array"
        item_array.add(i);
    }   
    
    public ObservableList<String> showPackageDetails (Package p) { // adds the information of a selected package to the ObservableList "package_list" and returns it to the classes "StorageViewController" and "TransportViewController"
        ObservableList<String> package_list = FXCollections.observableArrayList();
        ArrayList<Package> package_array = s.getPackageArray(); //gets the "package_array" from the "Storage" class
        for (Package a : package_array) {
            if (p.equals(a)) {
                package_list.add("Lähtöpaikka: " + a.sp_start.getLocation());
                package_list.add("Määränpää: " + a.sp_destination.getLocation());
                package_list.add("Kuljetusmatkan pituus: " + a.distance + " km");
                package_list.add("Paketin sisältö: " + a.item.toString());
            }
        }
        return package_list;
    }
    
    public void printLog() throws IOException { // creates a log of the packages in stock and transportation and saves it as a txt file
        int not_delivered_amount = 0;
        int delivered_amount = 0;
        File file = new File("loki.txt"); // defines the name of a txt file
        FileWriter writer = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        bufferedWriter.write("TIMO-järjestelmä / lokitiedosto" + System.getProperty("line.separator")+ System.getProperty("line.separator") + "Varastossa olevat paketit:" + System.getProperty("line.separator") + System.getProperty("line.separator"));
        ArrayList<Package> package_array = s.getPackageArray(); //gets the "package_array" from the "Storage" class
        for (Package a : package_array) { // finds the packages not delivered and writes their information to the log
            if (a.delivered == FALSE) {
                bufferedWriter.write("Paketin ID: " + a.id + System.getProperty("line.separator") + "Lähtöpaikka: " + a.sp_start.getLocation() + System.getProperty("line.separator") + "Määränpää: "
                        + a.sp_destination.getLocation() + System.getProperty("line.separator") + "Kuljetusmatkan pituus: " + a.distance + " km" + System.getProperty("line.separator") +
                        "Paketin sisältö: " + a.item.toString() + System.getProperty("line.separator") + System.getProperty("line.separator"));
                not_delivered_amount++;
            }
        }
        bufferedWriter.write("Varastossa yhteensä " + not_delivered_amount + " pakettia." + System.getProperty("line.separator") + System.getProperty("line.separator"));
        bufferedWriter.write("Toimitetut / toimituksessa olevat paketit:" + System.getProperty("line.separator")+ System.getProperty("line.separator"));
        for (Package a : package_array) { // finds the packages already delivered and writes their information to the log
            if (a.delivered == TRUE) {
                bufferedWriter.write("Paketin ID: " + a.id + System.getProperty("line.separator") + "Lähtöpaikka: " + a.sp_start.getLocation() + System.getProperty("line.separator") + "Määränpää: "
                        + a.sp_destination.getLocation() + System.getProperty("line.separator") + "Kuljetusmatkan pituus: " + a.distance + " km" + System.getProperty("line.separator") +
                        "Paketin sisältö: " + a.item.toString() + System.getProperty("line.separator") + System.getProperty("line.separator"));
                delivered_amount++;
            }
        }
        bufferedWriter.write("Toimitettu / toimituksessa yhteensä " + delivered_amount + " pakettia.");
        bufferedWriter.close();
    }
}

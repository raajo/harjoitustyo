/*
 * Program: TIMO
 * Filename: FXMLPackageInfoController.java
 * Author: Joonas Raassina
 * Content: The FXMLPackageInfoController java class which offers an FXML controller for the pop-up window which offers a user information about package classes
 */
package harjoitustyö;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;


public class FXMLPackageInfoController implements Initializable {

    @FXML
    private Button closeButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void closeAction(ActionEvent event) { // closes the window as the close button is selected
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
    
}

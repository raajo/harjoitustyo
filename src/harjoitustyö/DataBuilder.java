/*
 * Program: TIMO
 * Filename: DataBuilder.java
 * Author: Joonas Raassina
 * Content: The DataBuilder java class which parses the xml file and has ArrayLists for cities and SmartPost lockers
 */

package harjoitustyö;

import java.net.URL;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DataBuilder {
    static private DataBuilder d = null;
    private ArrayList<String> city_array = new ArrayList<String>(); //initializes an ArrayList for cities
    private ArrayList<SmartPost> smartpost_array = new ArrayList<SmartPost>(); //initializes an ArrayList for SmartPost lockers
    
    static public DataBuilder getInstance() {
        if (d == null)
            d = new DataBuilder();
        return d;
    }
    
    private DataBuilder() {
        try {	
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(new URL("http://smartpost.ee/fi_apt.xml").openStream());
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("place");
            for (int temp = 0; temp < nList.getLength(); temp++) {
		Node nNode = nList.item(temp);
		if (nNode.getNodeType() == Node.ELEMENT_NODE) {
			Element eElement = (Element) nNode;
                        GeoPoint g = new GeoPoint((Float.parseFloat(eElement.getElementsByTagName("lat").item(0).getTextContent())),
                                (Float.parseFloat(eElement.getElementsByTagName("lng").item(0).getTextContent()))); // creates a new "GeoPoint" object for the location of a SmartPost locker
                        smartpost_array.add(new SmartPost(eElement.getElementsByTagName("code").item(0).getTextContent(),
                                eElement.getElementsByTagName("city").item(0).getTextContent(),eElement.getElementsByTagName("address").item(0).getTextContent(),
                                eElement.getElementsByTagName("availability").item(0).getTextContent(),eElement.getElementsByTagName("postoffice").item(0).getTextContent(),
                                g)); // creates a new "SmartPost" object and adds it to the ArrayList "smartpost_array"
                        int match = 0;
                        for (String s : city_array) { //goes through the ArrayList "city_array" to avoid duplicates
                            if (s.equals(eElement.getElementsByTagName("city").item(0).getTextContent())) {
                                match += 1;
                            }
                        }
                        if (match == 0) { // if any duplicate is not found, the name of a city is added to the ArrayList "city_array"
                                city_array.add(eElement.getElementsByTagName("city").item(0).getTextContent());
                        }
		}
            }
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public ArrayList getCityArray() { //returns the ArrayList "city_array" for the other objects
        return city_array;
    }
    
    public ArrayList getSmartPostArray() { //returns the ArrayList "smartpost_array" for the other objects
        return smartpost_array;
    }
}

/*
 * Program: TIMO
 * Filename: MainController.java
 * Author: Joonas Raassina
 * Content: The MainController java class which offers an FXML controller for the tab functionality
 */
package harjoitustyö;

import harjoitustyö.FXMLDocumentController;
import harjoitustyö.StorageViewController;
import harjoitustyö.TransportViewController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;


public class MainController implements Initializable{

    @FXML private Tab tab1;
    @FXML private Tab tab2;
    @FXML private Tab tab3;
    @FXML private TabPane tabPane ;
    @FXML private FXMLDocumentController tab1ContentController ;
    @FXML private StorageViewController tab2ContentController ;
    @FXML private TransportViewController tab3ContentController ;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        tabPane.getSelectionModel().selectedItemProperty() // listens if a user changes the tab and initializes the content of the selected tab
        .addListener((obs, oldTab, newTab) -> {
            if (newTab == tab2) {
                tab2ContentController.initialize(arg0, arg1);
            }
            else if (newTab == tab3) {
                tab3ContentController.initialize(arg0, arg1);
            }
        });
    }
}
/*
 * Program: TIMO
 * Filename: GeoPoint.java
 * Author: Joonas Raassina
 * Content: The GeoPoint java class which includes the latitude and longitude coordinates of a SmartPoint locker and returns them to the other classes
 */
package harjoitustyö;


public class GeoPoint {
    private float lat;
    private float lng;
    
    public GeoPoint(float a, float b) {
        lat = a;
        lng = b;
    }
    
    public float getLat() { // returns the latitude coordinate of a SmartPost locker
        return lat;
    }
    
    public float getLng() { // returns the longitude coordinate of a SmartPost locker
        return lng;
    }
    
}

/*
 * Program: TIMO
 * Filename: Harjoitustyö.java
 * Author: Joonas Raassina
 * Content: The Harjoitustyö java class which is the main class of the program and contains the start method
 */
package harjoitustyö;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Harjoitustyö extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Main m = Main.getInstance();
        stage.setTitle("TIMO - ota tai jätä");
        Parent root = FXMLLoader.load(getClass().getResource("Main.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setOnCloseRequest(e -> {try { // when a user exits the program, the logging method of the class "Main" is called and all the windows are closed 
            m.printLog();
            } catch (IOException ex) {
                Logger.getLogger(Harjoitustyö.class.getName()).log(Level.SEVERE, null, ex);
            }
                Platform.exit();});
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}

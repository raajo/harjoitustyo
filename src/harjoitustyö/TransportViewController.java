/*
 * Program: TIMO
 * Filename: TransportViewController.java
 * Author: Joonas Raassina
 * Content: The TransportViewController java class which offers an FXML controller for the tab "Kuljetustiedot"
 */
package harjoitustyö;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;


public class TransportViewController implements Initializable {

    @FXML
    private ComboBox<Package> deliveryCombo;
    @FXML
    private Button checkDeliveryButton;
    @FXML
    private ListView<String> infoView;
    @FXML
    private Label infoLabel;
    
    Main m = null;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        m = Main.getInstance(); //gets the instance of the class "Main"
        // initializing the content of the tab
        infoView.getItems().clear();
        int package_amount = 0;
        deliveryCombo.getItems().removeAll(deliveryCombo.getItems());
        ArrayList<Package> delivery_array = m.getStorageArray();
        for (Package p : delivery_array) { //adds the items already delivered to the ComboBox
            if (p.delivered == TRUE) {
                deliveryCombo.getItems().add(p);
                package_amount++;
            }
        }
        infoLabel.setText("Paketteja toimitettu / toimituksessa: " + package_amount); //shows the user the amount of packages in transportation
        
    }    

    @FXML
    private void checkDeliveryAction(ActionEvent event) { // shows the user the specific information about the package selected in the ComboBox
        infoView.setItems(m.showPackageDetails(deliveryCombo.getValue()));
    }
    
}

/*
 * Program: TIMO
 * Filename: StorageViewController.java
 * Author: Joonas Raassina
 * Content: The StorageViewController java class which offers an FXML controller for the tab "Varastotilanne"
 */
package harjoitustyö;

import static java.lang.Boolean.FALSE;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;


public class StorageViewController implements Initializable {

    @FXML
    private ComboBox<Package> storageCombo;
    @FXML
    private Button checkStorageButton;
    @FXML
    private Label infoLabel;
    @FXML
    private ListView<String> infoView;
    
    Main m = null;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        m = Main.getInstance(); //gets the instance of the class "Main"
        // initializing the content of the tab
        infoView.getItems().clear();
        int package_amount = 0;
        storageCombo.getItems().removeAll(storageCombo.getItems());
        ArrayList<Package> storage_array = m.getStorageArray();
        for (Package p : storage_array) { //adds the items not delivered to the ComboBox
            if (p.delivered == FALSE) {
                storageCombo.getItems().add(p);
                package_amount++;
            }
        }
        infoLabel.setText("Paketteja varastossa: " + package_amount); //shows the user the amount of packages in the storage
    }    

    @FXML
    private void checkStorageAction(ActionEvent event) { // shows the user the specific information about the package selected in the ComboBox
       infoView.setItems(m.showPackageDetails(storageCombo.getValue()));
    }
    
}

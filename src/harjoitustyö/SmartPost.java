/*
 * Program: TIMO
 * Filename: SmartPost.java
 * Author: Joonas Raassina
 * Content: The SmartPost java class which represents a SmartPost locker
 */
package harjoitustyö;


public class SmartPost {
    private String code;
    private String city;
    private String address;
    private String availability;
    private String postoffice;
    private GeoPoint gp;
    
    public SmartPost(String a, String b, String c, String d, String e, GeoPoint h) {
        code = a;
        city = b;
        address = c;
        availability = d;
        postoffice = e;
        gp = h;
    }
    
    public String getCity() { // returns to the other classes the city where a SmartPost locker is located
        return city;
    }
    
    public float getLat() { // returns the latitude coordinate of a SmartPost locker to the other classes
        return gp.getLat();
    }
    
    public float getLng() { // returns the longitude coordinate of a SmartPost locker to the other classes
        return gp.getLng();
    }
    
    public String getLocation() { // returns the location of a SmartPost locker to the other classes
        return address + ", " + code + " " + city;
    }
    
    public String getInfo() { // returns the location and availability information of a SmartPost locker to the other classes
        return postoffice + ", " + availability;
    }
    
    @Override
    public String toString() { // returns to the other classes the post office where a SmartPost locker is located
        return postoffice;
    }
    
}
